//deps: jquery 1.7+
var config = {
	left: '.left .row',
	right: '.right .row'
}

function compensateHeights(config) {
	if (window.innerWidth > 1024) {
		var leftHeight = [],
			rightHeight = [];
		for (var i = 0; i < $(config.left).length; i++) {
			leftHeight[i] = $($(config.left)[i]).innerHeight();
		};
		for (var i = 0; i < $(config.right).length; i++) {
			rightHeight[i] = $($(config.right)[i]).innerHeight();
		};
		for (var i = 0; i < rightHeight.length; i++) {
			if (leftHeight[i] !== undefined) {
				if (leftHeight[i] > rightHeight[i]) {
					$($(config.right)[i]).innerHeight(leftHeight[i]);
				}
				if (leftHeight[i] < rightHeight[i]) {
					$($(config.left)[i]).innerHeight(rightHeight[i]);
				}
			}
		};
	}
}

$('#pre_order_form').on('submit', function() {
	setTimeout(function() {
		//timeout to wait ajax with errors
		compensateHeights(config);
	}, 100);
})
setTimeout(function() {
	compensateHeights(config);
},200);

//polling html change