var Popup =
{
    elName: {
        open: '.popupOpen',
        close: '.popupClose',
        popupcontainer: '.popupBaseWrap'
    },
    init: function()
    {
        $('body').on('click', Popup.elName.open, Popup.openPopup);
        $('body').on('click', Popup.elName.close, Popup.closePopup);
    },

    openPopup: function()
    {
        var popupId = $(this).attr('rel');
        if (popupId && $('#' + popupId).length > 0)
        {
            $('#' + popupId).height($('body').height()).show();
        }

        Popup.setPopupCenterPosition(popupId);

        return false;
    },

    closePopup: function()
    {
        var popup = $(this).parents(Popup.elName.popupcontainer);
        popup.hide();
        popup.find('.errorText').html('');
        return false;
    },

    setPopupCenterPosition: function(popup_id)
    {
        var popup_margin_top = Popup._getMarginTop(popup_id);
        $('#' + popup_id + ' .popupBase').css('margin-top', popup_margin_top + 'px');
    },

    _getMarginTop: function(popup_id)
    {
        var popup_base = $('#' + popup_id + ' .popupBase');

        var popup_height = popup_base.height();
        var window_height = $(window).height();
        var window_scroll_top = $(document).scrollTop();

        var popup_margin_top;
        if (window_height > popup_height)
        {
            popup_margin_top = ((window_height - popup_height) / 2) + window_scroll_top;
        }
        else
        {
            popup_margin_top = window_scroll_top;
        }

        return popup_margin_top;
    }
}
var RenderChanges =
{
    elName: '.renderChanges',
    init: function()
    {
        $(RenderChanges.elName).on('change', function() {
            var elRender = $($(this).attr('data-renderto'));
            if (elRender.length > 0)
            {
                elRender.html(RenderChanges.getContent($(this)));
            }
        });
        $(RenderChanges.elName).on('keyup', function() {
            var elRender = $($(this).attr('data-renderto'));
            if (elRender.length > 0)
            {
                elRender.html(RenderChanges.getContent($(this)));
            }
        });
    },
    getContent: function(el)
    {
        var content = el.html();
        var elTagName = el.prop("tagName").toLowerCase();
        switch(elTagName)
        {
            case 'input':
                var elType = el.attr("type").toLowerCase();
                switch (elType)
                {
                    case 'text':
                        content = el.val();
                        break;
                    case 'radio':
                        content = $("label[for='" + el.attr("id") + "']").html();
                        break;
                }
                break;
            case 'select':
                var elSelected = el.find('option:selected');
                content = (elSelected.val()) ? elSelected.html() : '';
                break;
            case 'textarea':
                content = el.html();
                break;
        }
        var arr = el.attr('data-rendertoarr');
        if (arr && content)
        {
            content += arr;
        }
        return content;
    }
}
var Devellar = {
    spoiler : function () {
        // description spoiler toggler
        var description = $('.DescriptionText');
        var uploadFiles = $('.uplBox.bidding');
        var actionButton = $('.titleBig');
        description.toggle(600);
        uploadFiles.toggle();
        (actionButton.html() == 'Description <span class="spoilerBtn spoiled"></span>') ? 
            actionButton.html('View order details: <span class="spoilerBtn"></span>') : 
            actionButton.html('Description <span class="spoilerBtn spoiled"></span>') ;
    }
}
$(document).ready(function() {
    Popup.init();
    RenderChanges.init();

    $(".tooltip").tooltip();

    $('input[type="date"]').prop('type', 'text').datepicker({ dateFormat: 'yy-mm-dd' });
    $('input[type="time"]').prop('type', 'text');

    $('div.datePicker').click(function(){
        var element = $(this).parent().find('.hasDatepicker');
        if (element.datepicker('widget').is(':hidden'))
        {
            element.datepicker('show');
        }
        else
        {
            element.datepicker('hide');
        }
    });

    $('a[name="login"].login').click(function(){
        $('#loginTop').css('display','block');
        $('.loginBlock .login').focus();
        return false;
    })

    $('.openContainer').on('click', function(){
        $(this).toggleClass("open");
    });

    $('.toggleContainer').on('click', function(){
        var container =  $(this).attr('data-togglecontainer');
        if (container && $(container).length > 0)
        {
            $(container).toggle();
        }
    });

    $('.activateContainer').on('click', function(){
        var container =  $(this).attr('data-acivatecontainer');
        if (container && $(container).length > 0)
        {
            $(container).toggleClass('activContainer');
        }
    });

    if (typeof($.fn.perfectScrollbar) == 'function')
    {
        $('.containerScroll').perfectScrollbar();
    }
    else
    {
        $('.containerScroll').css('overflow-y', 'scroll');
    }
});
// Constructor. 
// Create new Dklab_Realplexor object.
function Dklab_Realplexor(fullUrl, namespace, viaDocumentWrite)
{
	// Current JS library version.
	var VERSION = "1.32";

	// Detect current page hostname.
	var host = document.location.host;
	
	// Assign initial properties.
	if (!this.constructor._registry) this.constructor._registry = {}; // all objects registry
	this.version = VERSION;
	this._map = {};
	this._realplexor = null;
	this._namespace = namespace;
	this._login = null;
	this._iframeId = "mpl" + (new Date().getTime());
	this._iframeTag = 
		'<iframe'
		+ ' id="' + this._iframeId + '"'
		+ ' onload="' + 'Dklab_Realplexor' + '._iframeLoaded(&quot;' + this._iframeId + '&quot;)"'
		+ ' src="' + fullUrl + '?identifier=IFRAME&amp;HOST=' + host + '&amp;version=' + this.version + '"'
		+ ' style="position:absolute; visibility:hidden; width:200px; height:200px; left:-1000px; top:-1000px"' +
		'></iframe>';
	this._iframeCreated = false;
	this._needExecute = false;
	this._executeTimer = null;
	
	// Register this object in the registry (for IFRAME onload callback).
	this.constructor._registry[this._iframeId] = this;
	
	// Validate realplexor URL.
	if (!fullUrl.match(/^\w+:\/\/([^/]+)/)) {
		throw 'Dklab_Realplexor constructor argument must be fully-qualified URL, ' + fullUrl + ' given.';
	}
	var mHost = RegExp.$1;
	if (mHost != host && mHost.lastIndexOf("." + host) != mHost.length - host.length - 1) {
		throw 'Due to the standard XMLHttpRequest security policy, hostname in URL passed to Dklab_Realplexor (' + mHost + ') must be equals to the current host (' + host + ') or be its direct sub-domain.';
	} 
	
	// Create IFRAME if requested.
	if (viaDocumentWrite) {
		document.write(this._iframeTag);
		this._iframeCreated = true;
	}
	
	// Allow realplexor's IFRAME to access outer window.
	document.domain = host;	
}

// Static function. 
// Called when a realplexor iframe is loaded.
Dklab_Realplexor._iframeLoaded = function(id)
{
	var th = this._registry[id];
	// use setTimeout to let IFRAME JavaScript code some time to execute.
	setTimeout(function() {
		var iframe = document.getElementById(id);
		th._realplexor = iframe.contentWindow.Dklab_Realplexor_Loader;
		if (th.needExecute) {
			th.execute();
		}
	}, 50);
}

// Set active login.
Dklab_Realplexor.prototype.logon = function(login) {
	this._login = login;
}

// Set the position from which we need to listen a specified ID.
Dklab_Realplexor.prototype.setCursor = function(id, cursor) {
	if (!this._map[id]) this._map[id] = { cursor: null, callbacks: [] };
	this._map[id].cursor = cursor;
	return this;
}

// Subscribe a new callback to specified ID.
// To apply changes and reconnect to the server, call execute()
// after a sequence of subscribe() calls.
Dklab_Realplexor.prototype.subscribe = function(id, callback) {
	if (!this._map[id]) this._map[id] = { cursor: null, callbacks: [] };
	var chain = this._map[id].callbacks;
	for (var i = 0; i < chain.length; i++) {
		if (chain[i] === callback) return this;
	}
	chain.push(callback);
	return this;
}

// Unsubscribe a callback from the specified ID.
// You do not need to reconnect to the server (see execute()) 
// to stop calling of this callback.
Dklab_Realplexor.prototype.unsubscribe = function(id, callback) {
	if (!this._map[id]) return this;
	if (callback == null) {
		this._map[id].callbacks = [];
		return this;
	}
	var chain = this._map[id].callbacks;
	for (var i = 0; i < chain.length; i++) {
		if (chain[i] === callback) {
			chain.splice(i, 1);
			return this;
		}
	}
	return this;
}

// Reconnect to the server and listen for all specified IDs.
// You should call this method after a number of calls to subscribe().
Dklab_Realplexor.prototype.execute = function() {
	// Control IFRAME creation.
	if (!this._iframeCreated) {
		var div = document.createElement('DIV');
		div.innerHTML = this._iframeTag;
		document.body.appendChild(div);
		this._iframeCreated = true;
	}
	
	// Check if the realplexor is ready (if not, schedule later execution).
	if (this._executeTimer) {
		clearTimeout(this._executeTimer);
		this._executeTimer = null;
	}
	var th = this;
	if (!this._realplexor) {
		this._executeTimer = setTimeout(function() { th.execute() }, 30);
		return;
	}
	
	// Realplexor loader is ready, run it.
	this._realplexor.execute(
		this._map, 
		this.constructor._callAndReturnException, 
		(this._login != null? this._login + "_" : "") + (this._namespace != null? this._namespace : "")
	);
}

// This is a work-around for stupid IE. Unfortunately IE cannot
// catch exceptions which are thrown from the different frame
// (in most cases). Please see
// http://blogs.msdn.com/jaiprakash/archive/2007/01/22/jscript-exceptions-not-handled-thrown-across-frames-if-thrown-from-a-expando-method.aspx
Dklab_Realplexor._callAndReturnException = function(func, args) {
	try {
		func.apply(null, args);
		return null;
	} catch (e) {
		return "" + e;
	}
}

$.Dklab_Realplexor = {
    'initialized': false,
    'init': function(cometHost) {
        $.Dklab_Realplexor.client = new Dklab_Realplexor(cometHost);
        $.Dklab_Realplexor.initialized = true;
    },
    'getClient': function() {
        if ($.Dklab_Realplexor.initialized)
        {
            return $.Dklab_Realplexor.client;
        }
        else
        {
            $.error('$.Dklab_Realplexor not initialized!');
        }
    }
};

(function($) {
    
    var methods = {
        init: function (options) {
            var selObject = $(this);
            
            var defaultOptions = {
                'listenChannels': ['Alpha', 'Beta'],
                'sendUrl': window.location.protocol + '//' + window.location.hostname + '/chat/send',
                'cometHost': window.location.protocol + '//comet.' + window.location.hostname,
                'additionalParams': {},
                'listenParams': {},
                'onMessageReceive': function(channel, result) {
                    alert(result);
                },
                'onSuccessfulSend': function(channel, message) {
                    alert('Sent!');
                },
                'beforeSend': function(channel, message) {}
            };
            
            var parameters = $.extend(defaultOptions, options);
            
            if (typeof cometHost != 'undefined')
            {
                parameters.cometHost = cometHost;
            }

            if (!$.Dklab_Realplexor.initialized)
            {
                $.Dklab_Realplexor.init(parameters.cometHost);
            }
            
            var realplexor = $.Dklab_Realplexor.getClient();
  
            if (!selObject.data('realplexor'))
            {
                selObject.data('realplexor', parameters);
            }
            
            for (var i in parameters.listenChannels)
            {
                realplexor.subscribe(
                    parameters.listenChannels[i], 
                    function (result, channel) {
                        parameters.onMessageReceive(channel, result);
                    }
                );
            }
            
            realplexor.execute();
            
            return this;
        },
        unInitialize: function() {
            if (typeof $(this).data('realplexor') == 'object' && $(this).data('realplexor') != null)
            {
                var realplexor = $.Dklab_Realplexor.getClient();
                
                for (var i in $(this).data('realplexor').listenChannels)
                {
                    realplexor.unsubscribe(
                        $(this).data('realplexor').listenChannels[i], 
                        null
                    );
                }
                realplexor.execute();

                $(this).data('realplexor', null);
            }
        },
        sendMessage: function (params) {
            var selObject = $(this);
            
            if (typeof $(this).data('realplexor') != 'object')
            {
                $.error('jQuery.realplexor not initialized!');
            }
            if (typeof params != 'object')
            {
                $.error('jQuery.realplexor.sendMessage params should be an object!');
            }
            
            selObject.data('realplexor').beforeSend(selObject, params.message);
            
            $.post(
                $(this).data('realplexor').sendUrl, 
                params,
                function(result) {
                    if (result)
                    {
                        var response = $.parseJSON(result);
                        if (response.refresh)
                        {
                            location.reload(); 
                        }
                    }
                    
                    selObject.data('realplexor').onSuccessfulSend(params.channel, result);
                }
            );
        }
    };

    $.fn.realplexor = function(method) {
        if (methods[method])
        {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        }
        else if (typeof method == 'object' || !method)
        {
            return methods.init.apply(this, arguments);
        }
        else
        {
            $.error('Method' + method + ' does not exists on jQuery.realplexor');
        }
    }
}(jQuery));