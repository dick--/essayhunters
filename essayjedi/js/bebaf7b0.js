var JS_EVENT_NEW_AVAILABLE_ORDER_CREATE = 'js_event_new_available_order_create';
var JS_EVENT_NEW_AVAILABLE_ORDER_UPDATE = 'js_event_new_available_order_update';

;(function($) {

$.newAvailableOrder = function(options)
{
    var plugin = this;

    plugin.settings = {
        comet_channel : null,
        rout_writer_available_orders : null,
        el_available_orders_table_body : '.js_available_orders_table_body',
        el_new_available_orders_count : '.js_new_available_orders_count',
        css_new_available_order_class : 'js_new_available_order'
    };

    plugin.actions = {
        ORDER_CREATE : 'orderCreate',
        ORDER_ASSIGN_WRITER : 'orderAssignWriter',
        ORDER_CANCEL : 'orderCancel',
        ORDER_EDIT : 'orderEdit'
    };

    var init = function()
    {
        if (!options.comet_channel)
        {
            $.error('You must set comet channel property: "comet_channel"');
        }
        if (!options.rout_writer_available_orders)
        {
            $.error('You must set rout to writer available orders page property: "rout_writer_available_orders"');
        }

        plugin.settings = $.extend({}, plugin.settings, options);

        NewAvailableOrder.init();
    };

    /**
     * object NewAvailableOrder
     */
    var NewAvailableOrder =
    {
        init : function()
        {
            NewAvailableOrder.initRealplexor();
        },

        initRealplexor : function()
        {
            $(plugin.settings.el_new_available_orders_count).realplexor(
            {
                'listenChannels': [plugin.settings.comet_channel],
                'onMessageReceive': function(channel, result)
                {
                    if (NewAvailableOrder.isJSON(result))
                    {
                        NewAvailableOrder.runAction($.parseJSON(result));
                    }
                }
            });
        },

        runAction : function(response)
        {
            switch (response.action)
            {
                case plugin.actions.ORDER_CREATE:
                    NewAvailableOrderAction.createAction(response);
                    break;
                case plugin.actions.ORDER_ASSIGN_WRITER:
                    NewAvailableOrderAction.assignWriterAction(response);
                    break;
                case plugin.actions.ORDER_CANCEL:
                    NewAvailableOrderAction.cancelAction(response);
                    break;
                case plugin.actions.ORDER_EDIT:
                    NewAvailableOrderAction.editAction(response);
                    break;
            }
        },

        isJSON : function(str)
        {
            try
            {
                $.parseJSON(str);
                return true;
            }
            catch (e)
            {
                return false;
            }
        }
    };

    /**
     * object NewAvailableOrderAction
     */
    var NewAvailableOrderAction =
    {
        createAction : function(data)
        {
            NewAvailableOrderAction.checkOrderAvailability(data, plugin.actions.ORDER_CREATE);
        },

        assignWriterAction : function(data)
        {
            NewAvailableOrderAction.checkOrderAvailability(data, plugin.actions.ORDER_ASSIGN_WRITER);
        },

        cancelAction : function(data)
        {
            NewAvailableOrderAction.checkOrderAvailability(data, plugin.actions.ORDER_CANCEL);
        },

        editAction : function(data)
        {
            NewAvailableOrderAction.checkOrderAvailability(data, plugin.actions.ORDER_EDIT);
        },

        checkOrderAvailability : function(data, action)
        {
            var order_id = data.order.id;
            var is_user_on_available_orders_page = NewAvailableOrderAction.isUserOnAvailableOrdersPage();
            var salt = Math.random();

            var send_date = {order_id: order_id, available_orders_page : is_user_on_available_orders_page, salt: salt, action: action};
            var delay_time_to_start = Math.floor(Math.random() * 4000) + 1;

            setTimeout(function()
            {
                $.get(plugin.settings.rout_writer_available_orders, send_date, function(response)
                {
                    var getOrderRaw = function(order_id)
                    {
                        var order_raw = $(plugin.settings.el_available_orders_table_body + ' tr[order-id="' + order_id + '"]');
                        return (order_raw.length > 0) ? order_raw : null;
                    };

                    if (typeof response.is_order_available != 'undefined')
                    {
                        if (is_user_on_available_orders_page)
                        {
                            if (response.is_order_available)
                            {
                                var old_raw = getOrderRaw(order_id);
                                var new_raw = $($.parseHTML(response.content));

                                if (old_raw)
                                {
                                    if (old_raw.hasClass(plugin.settings.css_new_available_order_class))
                                    {
                                        new_raw.addClass(plugin.settings.css_new_available_order_class);
                                    }

                                    old_raw.replaceWith(new_raw);
                                }
                                else
                                {
                                    new_raw.addClass(plugin.settings.css_new_available_order_class);
                                    $(plugin.settings.el_available_orders_table_body).prepend(new_raw);
                                }
                            }
                            else
                            {
                                var old_raw = getOrderRaw(order_id);

                                if (old_raw)
                                {
                                    old_raw.remove();
                                }
                            }
                        }

                        NewAvailableOrderAction.checkDisplayNewAvailableOrdersCount(response.new_available_orders_count);
                        NewAvailableOrderAction.fireEvent(action, {order_raw_in_table: new_raw});
                    }
                });

            }, delay_time_to_start);
        },

        isUserOnAvailableOrdersPage : function()
        {
            return ($(plugin.settings.el_available_orders_table_body).length > 0) ? true : false;
        },

        checkDisplayNewAvailableOrdersCount : function(update_count)
        {
            var elem = $(plugin.settings.el_new_available_orders_count);

            if (NewAvailableOrderAction.isUserOnAvailableOrdersPage())
            {
                elem.hide();
            }
            else
            {
                var new_count = (update_count != undefined) ? parseInt(update_count, 10) : parseInt(elem.text(), 10);

                if (new_count > 0)
                {
                    elem.text('+' + new_count).show();
                }
                else
                {
                    elem.hide();
                }
            }
        },

        fireEvent : function(action, event_data)
        {
            switch (action)
            {
                case plugin.actions.ORDER_CREATE:
                    $(window).trigger(JS_EVENT_NEW_AVAILABLE_ORDER_CREATE, event_data);
                    break;
                case plugin.actions.ORDER_EDIT:
                    $(window).trigger(JS_EVENT_NEW_AVAILABLE_ORDER_UPDATE, event_data);
                    break;
            }
        }
    };

    $(function()
    {
        init();
    });
};

})(jQuery);