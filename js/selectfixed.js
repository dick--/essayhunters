$(document).ready(function() {
    $('.select-wrapper select').each(function(){
        var title = $('option:first', this).text();
        if( $('option:selected', this).text() != ''  ) title = $('option:selected',this).text();
        $(this)
            .css({'z-index':10,'opacity':0,'-khtml-appearance':'none'})
            .after('<span class="select">' + title + '</span>')
            .addClass('selTextRound')
            .addClass('select')
            .change(function(){
                val = $('option:selected',this).text();
                $(this).next().text(val);
            })
    });

    // // Only First selected option is color gray
    // $('.select-wrapper select').on("change", function(){
    //     function _getColor(el) {
    //         return $(el).next().css('color');
    //     }
    //     function _colorManage(el,color) {
    //         return $(el).next().css('color',color);
    //     }
    //     function _shadeRGBColor(color, percent) {
    //         /*http://stackoverflow.com/questions/5560248/programmatically-lighten-or-darken-a-hex-color-or-rgb-and-blend-colors*/
    //         var f=color.split(","),
    //             t=percent<0?0:255,
    //             p=percent<0?percent*-1:percent,
    //             R=parseInt(f[0].slice(4)),
    //             G=parseInt(f[1]),
    //             B=parseInt(f[2]);
    //         console.log(R);
    //         console.log(G);
    //         console.log(B);
    //         return "rgb("+(Math.round((t-R)*p)+R)+","+(Math.round((t-G)*p)+G)+","+(Math.round((t-B)*p)+B)+")";
    //     }
    //     if ( $(this).get(0).selectedIndex == 0 ) {
    //         console.log(_getColor($(this)));
    //         if (_getColor($(this)) !== undefined) {
    //             _colorManage(this,_shadeRGBColor(_getColor($(this)),0.3));
    //         } else {
    //             _colorManage(this,'#b1a9bb');
    //         }
            
    //     } else {
    //         //console.log('aaaa '+this);
    //         //FREE-2167 : #1
            
    //         ($(this).parents('.bodyRCB')).length > 0 ? _colorManage(this,'#7c859a') : _colorManage(this,'black');
    //     }
    // }).change();
})