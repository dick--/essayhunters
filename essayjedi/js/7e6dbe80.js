var DateChanger =
{
    dateFormat: 'dd M yy',
    elName: {
        base: '.dateChange',
        input: 'input.datechanger',
        data: 'span'
    },
    setDatePickerOptions: function(el)
    {
        if (el.attr('data-defaultdate'))
        {
            el.datepicker( "setDate", el.attr('data-defaultdate'));
        }
        $( ".selector" ).datepicker( "setDate", "10/12/2012" );
        if (el.attr('data-maxdate'))
        {
            el.datepicker( "option", "maxDate", new Date(el.attr('data-maxdate')));
        }
        if (el.attr('data-mindate'))
        {
            el.datepicker( "option", "minDate", new Date(el.attr('data-mindate')));
        }
    },
    toogleDatePicker: function(el)
    {
        if (el.datepicker('widget').is(':hidden'))
        {
            el.datepicker('show');
        }
        else
        {
            el.datepicker('hide');
        }
    },
    init: function()
    {
        $(DateChanger.elName.base + ' ' + DateChanger.elName.input).datepicker({
            dateFormat: 'yy-mm-dd',
            onSelect: function(dateText){
                $(DateChanger.elName.base + ' ' + DateChanger.elName.data).html($.datepicker.formatDate(DateChanger.dateFormat, new Date(dateText)));
            }
        });
        $(DateChanger.elName.base).on('click', function() {
            var dateChanger = $(this).find(DateChanger.elName.input);
            DateChanger.setDatePickerOptions(dateChanger);
            DateChanger.toogleDatePicker(dateChanger);
		});
    }
}

var DateRange =
{
    dateFormat: 'dd M yy',
    elName: {
        base: '.periodChange',
        datePickerContent: '.datePickerContent',
        datePicker: '.datePickerData',
        done: '.doneDatePicker',
        inputStart: 'input.datechanger_start',
        inputEnd: 'input.datechanger_end',
        dataStart: 'span.date_start',
        dataEnd: 'span.date_end'
    },
    init: function()
    {
        var elBase = $(DateRange.elName.base);
        var elDatePicker = $(DateRange.elName.datePicker, elBase);

        DateChanger.setDatePickerOptions(elDatePicker);

        $(DateRange.elName.done).on('click', function() {
            FilterForm.submitFormFromChild($(this));
            $(DateRange.elName.datePickerContent, $(this)).hide();
		});

        $(DateRange.elName.base + ' span').on('click', function() {
            var parentBase = $(this).parents(DateRange.elName.base);
            DateRange.initDatePicker(parentBase);
            $(DateRange.elName.datePickerContent, parentBase).toggle();
		});
    },
    initDatePicker: function(elBase)
    {
        var elStart = $(DateRange.elName.inputStart, elBase);
        var elEnd = $(DateRange.elName.inputEnd, elBase);
        var elDatePicker = $(DateRange.elName.datePicker, elBase);
        var elDoneButton = $(DateRange.elName.done, elBase);
        if (!elDatePicker.hasClass('hasDatepicker'))
        {
            elDatePicker.datepicker({
                dateFormat: 'yy-mm-dd',
                beforeShowDay: function(date) {
                    var date1 = $.datepicker.parseDate('yy-mm-dd', elStart.val());
                    var date2 = $.datepicker.parseDate('yy-mm-dd', elEnd.val());
                    return [true, date1 && ((date.getTime() == date1.getTime()) || (date2 && date >= date1 && date <= date2)) ? "dp-highlight" : ""];
                },
                onSelect: function(dateText, inst) {
                    var date1 = $.datepicker.parseDate('yy-mm-dd', elStart.val());
                    var date2 = $.datepicker.parseDate('yy-mm-dd', elEnd.val());
                    if (!date1 || date2) {
                        /* Select Start Date */
                        elStart.val(dateText);
                        elEnd.val("");
                        $(this).datepicker("option", "minDate", dateText);
                        elDoneButton.hide();
                        $(DateRange.elName.dataStart, elBase).html(
                            $.datepicker.formatDate(DateChanger.dateFormat, new Date($(this).val()))
                        );
                    } else {
                        /* Select End Date */
                        elEnd.val(dateText);
                        $(this).datepicker("option", "minDate", null);
                        elDoneButton.show();
                        $(DateRange.elName.dataEnd, elBase).html(
                            $.datepicker.formatDate(DateChanger.dateFormat, new Date($(this).val()))
                        );
                    }
                }
            });
        }
    }
}

var FilterForm =
{
    elName: '.filterForm',
    init: function()
    {
        $(FilterForm.elName).submit(FilterForm.onSubmit);
    },
    submitFormFromChild: function(childElement)
    {
        childElement.parents(FilterForm.elName).submit();
    },
    onSubmit: function()
    {
        var queryParams = FilterRedirect.getRequeryParams();
        for (var name in queryParams) {
            var nameParam = decodeURI(name);
            if ($('input[name="'+nameParam+'"], select[name="'+nameParam+'"]', $(this)).length == 0)
            {
                $(this).append($('<input/>').attr('type', 'hidden').attr('name', nameParam).val(queryParams[name]));
            }
        }
        return true;
    }
}

var FilterRedirect =
{
    getRequeryParams: function()
    {
        if (window.location.search)
        {
            var url = location.href;
            var qs = url.substring(url.indexOf('?') + 1).split('&');
            for(var i = 0, result = {}; i < qs.length; i++){
                qs[i] = qs[i].split('=');
                result[qs[i][0]] = decodeURIComponent(qs[i][1]);
            }

            return result;
        }
        else
        {
            return {};
        }
    }
}

$(document).ready(function(){
    DateChanger.init();
    DateRange.init();
    FilterForm.init();
});
var DropDownList = {
    elementClass: '.dropdownBasic',
    elementLinkClass: '.selectedItem',
    init: function() {
        this.initEvents();
        this.hide();
    },
    initEvents: function()
    {
        (function($) {
            $.each(['show','hide'], function(i, val) {
                var _org = $.fn[val];
                $.fn[val] = function() {
                    this.trigger(val);
                    _org.apply(this, arguments);
                };
            });
        })(jQuery);
        $(this.elementClass).bind('hide', function(){
            DropDownList.restoreValues(this);
        }).bind('show', function(){
                DropDownList.saveValues(this);
            })

        $(this.elementLinkClass).bind('click', function(){
            DropDownList.show($(this).siblings(this.elementClass));
        });
        $('html').click(function () {
            DropDownList.hide();
        });
        $(this.elementClass + ', ' + this.elementLinkClass + ',.checkboxStyle, .checkboxStyle + label').click(function (event) {
            event.stopPropagation();
        });
        $('input[type="checkbox"]', this.elementClass).on('change', function(e){
            if ($(this).val() == 0 && $(this).is(':checked'))
            {
                $('input[type="checkbox"][value!="0"]', $(this).parent().parent()).removeAttr('checked');
            }
            else
            {
                $('input[type="checkbox"][value="0"]', $(this).parent().parent()).removeAttr('checked');
            }
        })

    },
    show: function(element)
    {
        this.hide();
        $(element).fadeToggle(600);
    },
    hide: function()
    {
        $(this.elementClass).not(':hidden').hide();
    },
    saveValues: function(element)
    {
        var values = [];
        $('input[type="checkbox"]:checked', element).each(function(key, input){
            values.push($(input).val());
        })
        $(element).data('checked', values)
    },
    restoreValues: function(element)
    {
        var checkeds = $(element).data('checked');
        if (checkeds !== undefined && checkeds.length)
        {
            $('input[type="checkbox"]', element).prop('checked', false).filter(function(){
                return $.inArray($(this).val(), checkeds) !== -1;
            }).prop('checked', true);
        }
    }
}
;(function($)
{
    var plugin = function()
    {
    };

    plugin.prototype =
    {
        element : null,
        options : null,
        timer : undefined,
        gsecs : null,

        date_from : null,
        date_to : null,

        default_options :
        {
            step_in_seconds : 1,
            with_zero : true,
            format : "%%H%%:%%M%%:%%S%%", // "%%D%% Days, %%H%% Hours, %%M%% Minutes, %%S%% Seconds."
            is_utc_time : false,
            function_update_time : function(element, display_timer, seconds_left)
            {
                element.html(display_timer);
            },
            function_on_finish : function(){}
        },

        init : function(element, options)
        {
            this.element = element;
            this.options = $.extend(this.default_options, options);
        },

        start : function(date_to, date_from)
        {
            this._setDates(date_to, date_from);

            var time_out_period = this.options.step_in_seconds * 1000;

            var d_now = new Date(this.date_from);
            var d_then = new Date(this.date_to);

            var d_diff = new Date(d_then - d_now);
            this.gsecs = Math.floor(d_diff.valueOf() / 1000);

            this.stop();
            this.timer = setInterval($.proxy(this._countBack, this), time_out_period)
        },

        stop : function()
        {
            if (typeof this.timer != 'undefined')
            {
                clearInterval(this.timer);
            }
        },

        _setDates: function(date_to, date_from)
        {
            if (date_to != undefined)
            {
                this.date_to = date_to;
            }
            if (date_from != undefined)
            {
                this.date_from = date_from;
            }

            if (this.date_to == null)
            {
                $.error('On start count down parameter "date_to" was not set');
            }
            if (this.date_from == null)
            {
                var now = this.date_from = new Date();

                if (this.options.is_utc_time)
                {
                    this.date_from = new Date(now.getUTCFullYear(), now.getUTCMonth(), now.getUTCDate(),
                                                now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds());
                }
            }
        },

        _countBack: function()
        {
            this.gsecs = this.gsecs + this.options.step_in_seconds * -1;

            var display_timer = this.options.format.replace(/%%D%%/g, this._calcage(this.gsecs,86400,100000));
            display_timer = display_timer.replace(/%%H%%/g, this._calcage(this.gsecs,3600,24));
            display_timer = display_timer.replace(/%%M%%/g, this._calcage(this.gsecs,60,60));
            display_timer = display_timer.replace(/%%S%%/g, this._calcage(this.gsecs,1,60));

            this.options.function_update_time(this.element, display_timer, this.gsecs);

            if (this.gsecs <= 0)
            {
                this.stop();
                this.options.function_on_finish();
            }
        },

        _calcage: function(secs, num1, num2)
        {
            var num = ((Math.floor(secs/num1)) % num2).toString();

            if (isNaN(num))
            {
                num = '00';
            }
            else if (this.options.with_zero && num.length < 2)
            {
                num = '0' + num;
            }

            return num;
        }
    };

    var countDown = function(element, options)
    {
        var plug = new plugin();
        plug.init(element, options);

        this.init = $.proxy(plug, 'init');
        this.start = $.proxy(plug, 'start');
        this.stop = $.proxy(plug, 'stop');
    };

    $.fn.countDown = function(options)
    {
        var element = $(this);
        var old_count_down = element.data('countDown');
        var count_down;

        if (typeof old_count_down == 'object')
        {
            count_down = old_count_down;

            if (typeof options == 'object')
            {
                count_down.init(element, options);  // reinit
            }
        }
        else
        {
            count_down = new countDown(element, options);
            element.data('countDown', count_down);
        }

        return count_down;
    };

})(jQuery);
$(document).ready(function () {
    DropDownList.init();
    OrderDeadlineCountDown.init();
    $('#cb-online').on('change', function(e)
        {
            $('.filterForm').submit();
        }
    );
    var selected_subjects = [];
    var selected_types = [];
    $('.subject.filters input:checked').next().each(function(key, value){
            selected_subjects.push($(value).text());
        }
    );
    $('.ammount.filters input:checked').next().each(function(key, value){
            selected_types.push($(value).text());
        }
    );
    $('#selectedSubjects').append(selected_subjects.join(', '));
    $('#selectedTypes').append(selected_types.join(', '));
});

/**
 * OrderDeadlineCountDown object
 */
var OrderDeadlineCountDown =
{
    el_order_deadline_date : '.js_order_deadline_date',
    el_now_date : '.js_now_date',
    el_time_left : '.js_time_left',

    init_from_seconds : 24 * 60 * 60 * 1.2,     // 1.2 days
    update_from_seconds : 24 * 60 * 60,         // 1 day

    init : function()
    {
        $(OrderDeadlineCountDown.el_order_deadline_date).each(function()
        {
            OrderDeadlineCountDown.initCountDown($(this));
        });

        $(window).on(JS_EVENT_NEW_AVAILABLE_ORDER_CREATE + ' ' + JS_EVENT_NEW_AVAILABLE_ORDER_UPDATE, function(e, data)
        {
            if (typeof data.order_raw_in_table != 'undefined')
            {
                var element_order_raw_in_table = data.order_raw_in_table;
                var order_deadline_date = element_order_raw_in_table.find(OrderDeadlineCountDown.el_order_deadline_date);
                OrderDeadlineCountDown.initCountDown(order_deadline_date);
            }
        });
    },

    initCountDown : function(element)
    {
        var deadline_date = new Date(element.text());
        var now = new Date(element.siblings(OrderDeadlineCountDown.el_now_date).text());

        var date_diff = new Date(deadline_date - now);
        var seconds_left = Math.floor(date_diff.valueOf() / 1000);

        if (seconds_left > 0 && seconds_left <= OrderDeadlineCountDown.init_from_seconds)
        {
            var time_left_elem = element.siblings(OrderDeadlineCountDown.el_time_left);

            time_left_elem.countDown({
                    format:"%%H%%",
                    with_zero : false,
                    step_in_seconds: 15,
                    function_update_time: OrderDeadlineCountDown._updateDeadlineLeft
                }).start(deadline_date, now);
        }
    },

    _updateDeadlineLeft : function(element, hours_left, seconds_left)
    {
        if (seconds_left <= OrderDeadlineCountDown.update_from_seconds)
        {
            var str;

            if (seconds_left > 0)
            {
                if (hours_left == 0)
                {
                    hours_left = 1;
                }

                var add_s = (hours_left > 1) ? 'S' : '';
                str = '<span>' + hours_left + '</span> HOUR' + add_s + ' LEFT';
            }
            else
            {
                str = '<span>0</span> DAYS LEFT';
            }

            element.html(str)
        }
    }
};