var OrderForm = {
    filesRequiredService: [],
    pageWords: 275,
    maxPage: 9999,
    elName: {
        pages: '#order_product_pages',
        words: '#order_form_words',
        deadline: '#order_deadline',
        service: '#order_product_service',
        additionalMaterials: '#order_additional_materials'
    },
    initialize : function()
	{
        var elPages = $(OrderForm.elName.pages);
        var elWords = $(OrderForm.elName.words)
        if (elPages.length > 0 && elWords.length > 0)
        {
            elPages.on('keyup', function(){
                var count = OrderForm.getWordsCount(parseInt($(this).val()));
                elWords.html(count);
            });
            elPages.on('change', function(){
                var count = OrderForm.getWordsCount(parseInt($(this).val()));
                elWords.html(count);
            });
        }
        elPages.change();
        OrderForm.initDeadline();
        OrderForm.initDeadlineChanges();
        OrderForm.initService();
    },
    initService: function()
    {
        var elService = $(OrderForm.elName.service);
        var elAdditionalMaterials = $(OrderForm.elName.additionalMaterials);
        elService.find('input').on('change', function(){
            elAdditionalMaterials.removeAttr('required');
            if ($(this).is(':checked') && OrderForm.filesRequiredService.length > 0 && $.inArray(parseInt($(this).val()), OrderForm.filesRequiredService) !== -1)
            {
                elAdditionalMaterials.attr('required', 'required');
            }
            elAdditionalMaterials.change();
        });
    },
    initDeadline: function()
    {
        var deadlineEl = $('div.datePicker').parent().find('.hasDatepicker');
        deadlineEl.datepicker("option", { minDate: new Date() });
        if (!deadlineEl.datepicker('getDate'))
        {
            deadlineEl.datepicker('setDate', 'c+10d');
        }
    },
    initDeadlineChanges: function()
    {
        var elDeadline = $(OrderForm.elName.deadline);
        if (elDeadline.length > 0)
        {
            var elDeadlineTime = $('select', elDeadline);
            var elDeadlineDate = $('input', elDeadline);

            elDeadlineTime.on('change', function() {
                OrderForm.updateDeadlineViewData(elDeadline, $(this), elDeadlineDate);
            });
            elDeadlineDate.on('change', function() {
                OrderForm.updateDeadlineViewData(elDeadline, elDeadlineTime, $(this));
            });
        }
    },
    getWordsCount: function(page_count)
    {
        return (page_count > OrderForm.maxPage) ? '' : page_count * OrderForm.pageWords;
    },
    updateDeadlineViewData: function(elDeadline, elTime, elDate)
    {
        var deadlineDate = new Date();
        deadlineDate.setTime(Date.parse(elDate.val()));

        var deadlineData = elDeadline.attr('data-renderto');
        if (deadlineData && $(deadlineData).length > 0)
        {
            var formatedTime = elTime.val().replace(/:\d\d\s/g, '').replace(' ', '').toLowerCase();
            $(deadlineData).html($.datepicker.formatDate('dd M', deadlineDate) + ', ' + formatedTime);
        }
        var deadlineDataDateLeft = elDeadline.attr('data-rendertodateleft');
        if (deadlineDataDateLeft && $(deadlineDataDateLeft).length > 0)
        {
            var now = new Date();
            var dateLeft = Math.floor((deadlineDate.getTime() - now.getTime()) / 1000 / 60 / 60 / 24);
            $(deadlineDataDateLeft).html((dateLeft < 0) ? 0 : dateLeft);
        }
    }
}

var OrderFormInit = function(){
    OrderForm.initialize();
}

$(document).ready(function(){
	if (typeof(OrderFormExternalInit) == 'undefined' || OrderFormExternalInit == false ){
		OrderFormInit();
	}
})