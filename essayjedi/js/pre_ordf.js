jQuery(function() {
    var jsfv = new function () {
        function getComputeMessage(key, placeholders, number) {
            Translator.placeHolderPrefix = '{{ ';
            Translator.placeHolderSuffix = ' }}';
            // Default translations
            if (!Translator.has('validators:'+key)) 
            {
                Translator.add('validators:'+key, key);
            }

            return Translator.get('validators:'+key, placeholders, number);
        }
        
        function isNotDefined(value) {
            return (typeof(value) == 'undefined' || null === value || '' === value);
        }
        
        function checkError(field, checkFunction, parameters, value) {
            field = jsfv.id(field);
            jsfv.removeErrors(field);

            errorMessage = checkFunction((value === undefined ? field : value), parameters);

            if (errorMessage != true) 
            {
                jsfv.addError(field, errorMessage);

                return false;
            }

            return true;
        }
        
                    function NotBlank(field, params)
{
    var value = field && field.nodeName ? $(field).val() : field;

    if (isNotDefined(value)) {
        return getComputeMessage(params.message);
    }

    return true;
}                    function Email(field, params)
{
    var value = field && field.nodeName ? $(field).val() : field;

    if (isNotDefined(value)) {
        return true;
    }

    value = String(value);
    var pattern = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})$/;

    if ( pattern.test(value) ) {
        if (params && params.checkMX && field.nodeName) {
            //TODO This part is compartible only with jquery
            var cache, cacheindex = 'm' + value + '';
            cache = $(field).data('_checkMXCache');
            if (!cache) cache = {};
            if (cache[cacheindex] != undefined) {
                if (cache[cacheindex] === false) {
                    return getComputeMessage(params.message);
                } else return true;
            }
            $.ajax({
                type: 'POST',
                url: '\x2Fjsfv\x2Fcheckmx',
                data: {address: value.replace(new RegExp("^.*@"), "")},
                success: function(data, textStatus) {
                    if (data.status && data.status == 'ok') {
                        cache[cacheindex] = data.result;
                        $(field).data('_checkMXCache', cache);
                        if (!cache[cacheindex]) {
                            checkError($(field).attr('id'), Email, params);
                        }
                    }
                    },
                dataType: 'json'
            });
            }
        return true;
    }

    return getComputeMessage(params.message);
}                
                
        return {
            id: function (id) {
                return document.getElementById(id);
            },
            removeErrors: function (field) {

                var form = $(field).closest('form');
                

                if ($(field).next().is('.errorText'))
                {
                    var errorsHandler = $(field).next();
                }
                else if ($(field).parent().next().is('.errorText'))
                {
                    var errorsHandler = $(field).parent().next();
                }
                else
                {
                    var errorsHandler = $('.errorText', field);
                }
                $('ul.error_list', errorsHandler).remove();
            },
            addError: function (field, errorMessage) {
                // Add errors block
                field = $(field);

                if ($(field).next().is('.errorText'))
                {
                    var errorsHandler = $(field).next();
                }
                else if ($(field).parent().next().is('.errorText'))
                {
                    var errorsHandler = $(field).parent().next();
                }
                else
                {
                    var errorsHandler = $('.errorText', field);
                }

                if ($('ul.error_list', errorsHandler).length == 0)  {
                    if (errorsHandler.is('.errorText'))
                    {
                        errorsHandler.append('<ul class="error_list"></ul>');
                    }
                }

                // Add error
                $('.error_list', errorsHandler).append('<li />').find('li').last().html(errorMessage);
            },
            onEvent: function (field, eventType, handler) {
                if (typeof(field) == 'string') {
                    field = jsfv.id(field);
                }
                $(field).bind(eventType, handler);
            },
                            check_pre_order_form_customer_email: function() {
                    var gv;
                    result = true;
                                            result = result && checkError('pre_order_form_customer_email', NotBlank, {message:"Required Field"} );
                                            result = result && checkError('pre_order_form_customer_email', Email, {message:"Incorrect Format", checkMX:true, checkHost:false} );
                                                                                                                            return result;
                },
                        onReady: function() {
                $('form').data('blur_validation_disabled', false);
                
                $('form').on('click', 'input[type=submit]', function(event) {
                    $('form').data('blur_validation_disabled', true);
                });
            
                                    var form = jsfv.id('pre_order_form');

                    if (form) 
                    {
                        if ( form.nodeName.toLowerCase() != 'form') 
                        {
                            form = jsfv.id('pre_order_form__token').form;
                        }
                        
                        jsfv.onEvent(form, 'submit', function() {
                            var gv, submitForm = true;
                            $('form').data('blur_validation_disabled', false);
                                                            submitForm = jsfv.check_pre_order_form_customer_email() && submitForm;
                                                                                                                                                            return submitForm;
                        });
                    }
                                                                            var validator = function() {
                            var field = 'pre_order_form_customer_email';
                            
                            if (typeof(field) == 'string') {
                                field = jsfv.id(field);
                            }
                            
                            var disabled = $(field).closest('form').data('blur_validation_disabled');

                            if (!disabled) {
                                jsfv.check_pre_order_form_customer_email;
                            }
                        }
                    
                        jsfv.onEvent('pre_order_form_customer_email', 'blur', validator);
                                                                                                jsfv.onEvent('pre_order_form_customer_email', 'change propertychange', jsfv.check_pre_order_form_customer_email);
                                                            
                var validation = function validation() {
                    var form = jsfv.id('pre_order_form');
                    var gv, submitForm = true;
                                                submitForm = jsfv.check_pre_order_form_customer_email() && submitForm;
                    
                    return submitForm;
                }
                
                $(form).data('validation', validation);
            }
        };
    }
    
    $(jsfv.onReady);
})