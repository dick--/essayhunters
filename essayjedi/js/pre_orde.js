jQuery(function() {
    var jsfv = new function () {
        function getComputeMessage(key, placeholders, number) {
            Translator.placeHolderPrefix = '{{ ';
            Translator.placeHolderSuffix = ' }}';
            // Default translations
            if (!Translator.has('validators:'+key)) 
            {
                Translator.add('validators:'+key, key);
            }

            return Translator.get('validators:'+key, placeholders, number);
        }
        
        function isNotDefined(value) {
            return (typeof(value) == 'undefined' || null === value || '' === value);
        }
        
        function checkError(field, checkFunction, parameters, value) {
            field = jsfv.id(field);
            jsfv.removeErrors(field);

            errorMessage = checkFunction((value === undefined ? field : value), parameters);

            if (errorMessage != true) 
            {
                jsfv.addError(field, errorMessage);

                return false;
            }

            return true;
        }
        
                    function NotBlank(field, params)
{
    var value = field && field.nodeName ? $(field).val() : field;

    if (isNotDefined(value)) {
        return getComputeMessage(params.message);
    }

    return true;
}                    function Length(field, params)
{
    var value = field && field.nodeName ? $(field).val() : field;

    if (isNotDefined(value)) {
        return true;
    }

    value = String(value);
	
	if (params['exactMessage'] !== undefined && 
	    params['min'] !== undefined && params['max'] !== undefined && 
	    params.min == params.max) {
	    if (value.length != parseInt(params.min)) {
    		return getComputeMessage(params.exactMessage, { 'limit' : params.min });
	    }
    } else {
	    if (params['min'] !== undefined && value.length < parseInt(params.min)) {
	        return getComputeMessage(params.minMessage, { 'limit' : params.min });
	    }
	    if (params['max'] !== undefined && value.length > parseInt(params.max)) {
	        return getComputeMessage(params.maxMessage, { 'limit' : params.max });
	    }
    }

    return true;
}
                    function Type(field, params)
{
    var result = true;

    var value = field && field.nodeName ? $(field).val() : field;

    switch (params.type)
    {
        case 'numeric':
            result = result & $.isNumeric(value);
            break;
        case 'double':
            result = result & $.isNumeric(value);
            break;
    }

    if (!result)
    {
        return getComputeMessage(params.message, { 'type' : params.type } );
    }

    return result;
}                    function Range(field, params)
{
    var value = field && field.nodeName ? $(field).val() : field;

    if (isNotDefined(value)) {
        return true;
    }

    value = parseFloat(value);

	if (value < parseFloat(params.min)) {
        return getComputeMessage(params.minMessage, { 'limit' : params.min });
    }
    if (value > parseFloat(params.max)) {
        return getComputeMessage(params.maxMessage, { 'limit' : params.max } );
    }

    return true;
}
                    function Deadline(field, params)
{
    try
    {
        var currentdate = new Date();

        currentdate.setTime(Date.parse(currentServerDate));

        var timestamp = Math.round(currentdate.getTime()/1000);

        var value = field && field.nodeName ? $(field).val() : field;

        var timeValue = $('select[name*="[time]"]', field).val();
        var dateValue = $('input[name*="[date]"]', field).val();

        if (!$('select[name*="[time]"]', field).val()
                || !$('input[name*="[date]"]', field).val())
        {
            return getComputeMessage(params.message, { });
        }

        $('select[name*="[time]"], input[name*="[date]"]', field).bind('change, blur', function(){
            $(this).parent().blur().change();
        })

        var timeShift = parseInt('10800');

        var deadline = $.datepicker.parseDate('yy-mm-dd', dateValue);

        if (deadline instanceof Date)
        {
            if (timeValue.toLowerCase().indexOf('am') != '-1')
            {
                var AmPmPosition = timeValue.toLowerCase().indexOf('am');
                var hours_pm = 0;
            }
            else if (timeValue == '12:00 PM')
            {
                var AmPmPosition = timeValue.toLowerCase().indexOf('pm');
                var hours_pm = 0;
            }
            else
            {
                var AmPmPosition = timeValue.toLowerCase().indexOf('pm');
                var hours_pm = 12;
            }

            var time = timeValue.substring(0, AmPmPosition);
            var hours = parseInt(time.substring(0, timeValue.toLowerCase().indexOf(':'))) + parseInt(hours_pm);
            var minutes = parseInt(time.substring(timeValue.toLowerCase().indexOf(':') + 1));
            
            deadline.setHours(hours, minutes);

            if (deadline.getTime() < currentdate.getTime() + timeShift * 1000)
            {
                return getComputeMessage(params.message, { });
            }
        }
        else
        {
            return getComputeMessage(params.message, { });
        }
        return true;
    }
    catch (err)
    {
        return getComputeMessage(params.messageIncorectFormat, { });
    }
}
                
                
        return {
            id: function (id) {
                return document.getElementById(id);
            },
            removeErrors: function (field) {

                var form = $(field).closest('form');
                

                if ($(field).next().is('.errorText'))
                {
                    var errorsHandler = $(field).next();
                }
                else if ($(field).parent().next().is('.errorText'))
                {
                    var errorsHandler = $(field).parent().next();
                }
                else
                {
                    var errorsHandler = $('.errorText', field);
                }
                $('ul.error_list', errorsHandler).remove();
            },
            addError: function (field, errorMessage) {
                // Add errors block
                field = $(field);

                if ($(field).next().is('.errorText'))
                {
                    var errorsHandler = $(field).next();
                }
                else if ($(field).parent().next().is('.errorText'))
                {
                    var errorsHandler = $(field).parent().next();
                }
                else
                {
                    var errorsHandler = $('.errorText', field);
                }

                if ($('ul.error_list', errorsHandler).length == 0)  {
                    if (errorsHandler.is('.errorText'))
                    {
                        errorsHandler.append('<ul class="error_list"></ul>');
                    }
                }

                // Add error
                $('.error_list', errorsHandler).append('<li />').find('li').last().html(errorMessage);
            },
            onEvent: function (field, eventType, handler) {
                if (typeof(field) == 'string') {
                    field = jsfv.id(field);
                }
                $(field).bind(eventType, handler);
            },
                            check_pre_order_form_order_type: function() {
                    var gv;
                    result = true;
                                            result = result && checkError('pre_order_form_order_type', NotBlank, {message:"It is an obligatory field. Please fill it in."} );
                                                                                                                            return result;
                },
                            check_pre_order_form_order_subject: function() {
                    var gv;
                    result = true;
                                            result = result && checkError('pre_order_form_order_subject', NotBlank, {message:"It is an obligatory field. Please fill it in."} );
                                                                                                                            return result;
                },
                            check_pre_order_form_order_name: function() {
                    var gv;
                    result = true;
                                            result = result && checkError('pre_order_form_order_name', NotBlank, {message:"It is an obligatory field. Please fill it in."} );
                                            result = result && checkError('pre_order_form_order_name', Length, {maxMessage:"This value is too long. It should have {{ limit }} character or less.|This value is too long. It should have {{ limit }} characters or less.", minMessage:"This value is too short. Type in {{ limit }} characters or more.", exactMessage:"This value should have exactly {{ limit }} character.|This value should have exactly {{ limit }} characters.", max:255, min:4, charset:"UTF-8"} );
                                                                                                                            return result;
                },
                            check_pre_order_form_order_pages: function() {
                    var gv;
                    result = true;
                                            result = result && checkError('pre_order_form_order_pages', NotBlank, {message:"Required Field."} );
                                            result = result && checkError('pre_order_form_order_pages', Type, {message:"Incorrect Data or Format", type:"numeric"} );
                                            result = result && checkError('pre_order_form_order_pages', Range, {minMessage:"This value should be {{ limit }} or more.", maxMessage:"This value should be {{ limit }} or less.", invalidMessage:"This value should be a valid number.", min:1, max:9999} );
                                                                                                                            return result;
                },
                            check_pre_order_form_order_deadline: function() {
                    var gv;
                    result = true;
                                            result = result && checkError('pre_order_form_order_deadline', Deadline, {message:"Choose 3 Hours or More", messageChangeDeadline:"Deadline can not be reduced.", messageIncorectFormat:"Incorrect Data or Format"} );
                                                                                                                            return result;
                },
                        onReady: function() {
                $('form').data('blur_validation_disabled', false);
                
                $('form').on('click', 'input[type=submit]', function(event) {
                    $('form').data('blur_validation_disabled', true);
                });
            
                                    var form = jsfv.id('pre_order_form');

                    if (form) 
                    {
                        if ( form.nodeName.toLowerCase() != 'form') 
                        {
                            form = jsfv.id('pre_order_form__token').form;
                        }
                        
                        jsfv.onEvent(form, 'submit', function() {
                            var gv, submitForm = true;
                            $('form').data('blur_validation_disabled', false);
                                                            submitForm = jsfv.check_pre_order_form_order_type() && submitForm;
                                                            submitForm = jsfv.check_pre_order_form_order_subject() && submitForm;
                                                            submitForm = jsfv.check_pre_order_form_order_name() && submitForm;
                                                            submitForm = jsfv.check_pre_order_form_order_pages() && submitForm;
                                                            submitForm = jsfv.check_pre_order_form_order_deadline() && submitForm;
                                                                                                                                                            return submitForm;
                        });
                    }
                                                                            var validator = function() {
                            var field = 'pre_order_form_order_type';
                            
                            if (typeof(field) == 'string') {
                                field = jsfv.id(field);
                            }
                            
                            var disabled = $(field).closest('form').data('blur_validation_disabled');

                            if (!disabled) {
                                jsfv.check_pre_order_form_order_type;
                            }
                        }
                    
                        jsfv.onEvent('pre_order_form_order_type', 'blur', validator);
                                            var validator = function() {
                            var field = 'pre_order_form_order_subject';
                            
                            if (typeof(field) == 'string') {
                                field = jsfv.id(field);
                            }
                            
                            var disabled = $(field).closest('form').data('blur_validation_disabled');

                            if (!disabled) {
                                jsfv.check_pre_order_form_order_subject;
                            }
                        }
                    
                        jsfv.onEvent('pre_order_form_order_subject', 'blur', validator);
                                            var validator = function() {
                            var field = 'pre_order_form_order_name';
                            
                            if (typeof(field) == 'string') {
                                field = jsfv.id(field);
                            }
                            
                            var disabled = $(field).closest('form').data('blur_validation_disabled');

                            if (!disabled) {
                                jsfv.check_pre_order_form_order_name;
                            }
                        }
                    
                        jsfv.onEvent('pre_order_form_order_name', 'blur', validator);
                                            var validator = function() {
                            var field = 'pre_order_form_order_pages';
                            
                            if (typeof(field) == 'string') {
                                field = jsfv.id(field);
                            }
                            
                            var disabled = $(field).closest('form').data('blur_validation_disabled');

                            if (!disabled) {
                                jsfv.check_pre_order_form_order_pages;
                            }
                        }
                    
                        jsfv.onEvent('pre_order_form_order_pages', 'blur', validator);
                                            var validator = function() {
                            var field = 'pre_order_form_order_deadline';
                            
                            if (typeof(field) == 'string') {
                                field = jsfv.id(field);
                            }
                            
                            var disabled = $(field).closest('form').data('blur_validation_disabled');

                            if (!disabled) {
                                jsfv.check_pre_order_form_order_deadline;
                            }
                        }
                    
                        jsfv.onEvent('pre_order_form_order_deadline', 'blur', validator);
                                                                                                jsfv.onEvent('pre_order_form_order_type', 'change propertychange', jsfv.check_pre_order_form_order_type);
                                            jsfv.onEvent('pre_order_form_order_subject', 'change propertychange', jsfv.check_pre_order_form_order_subject);
                                            jsfv.onEvent('pre_order_form_order_name', 'change propertychange', jsfv.check_pre_order_form_order_name);
                                            jsfv.onEvent('pre_order_form_order_pages', 'change propertychange', jsfv.check_pre_order_form_order_pages);
                                            jsfv.onEvent('pre_order_form_order_deadline', 'change propertychange', jsfv.check_pre_order_form_order_deadline);
                                                            
                var validation = function validation() {
                    var form = jsfv.id('pre_order_form');
                    var gv, submitForm = true;
                                                submitForm = jsfv.check_pre_order_form_order_type() && submitForm;
                                                submitForm = jsfv.check_pre_order_form_order_subject() && submitForm;
                                                submitForm = jsfv.check_pre_order_form_order_name() && submitForm;
                                                submitForm = jsfv.check_pre_order_form_order_pages() && submitForm;
                                                submitForm = jsfv.check_pre_order_form_order_deadline() && submitForm;
                    
                    return submitForm;
                }
                
                $(form).data('validation', validation);
            }
        };
    }
    
    $(jsfv.onReady);
})